#ifndef _VARIANT_ARDUINO_STM32_
#define _VARIANT_ARDUINO_STM32_

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

// This must be a literal
#define NUM_DIGITAL_PINS        57

// Timer Definitions
// Use TIM6/TIM7 when possible as servo and tone don't need GPIO output pin
#define TIMER_TONE              TIM6
#define TIMER_SERVO             TIM7

// CAN Definitions
#define CAN_RX                  40
#define CAN_TX                  41

// SPI Definitions
#define MOSI                    42                  
#define MISO                    43
#define SCK                     44
#define CS1                     45 
#define CS2                     46  
#define CS3                     47  
#define CS4                     48  
#define CS5                     49  
#define CS6                     50  
#define CS7                     51  

// Serial definitions
#define USART3_RX               52
#define USART3_TX               53
#define USART2_RX               54    
#define USART2_TX               55

// BTN for testing only
#define BTN 56

// Default pin used for 'Serial' instance (ex: ST-Link)
#define SERIAL_UART_INSTANCE    3
#define PIN_SERIAL_RX           USART3_RX
#define PIN_SERIAL_TX           USART3_TX

#define HAL_CAN_MODULE_ENABLED

#ifdef __cplusplus
} // extern "C"
#define SERIAL_PORT_MONITOR     Serial
#define SERIAL_PORT_HARDWARE    Serial
#endif

#endif /* _VARIANT_ARDUINO_STM32_ */
