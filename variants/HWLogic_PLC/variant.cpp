#include "variant.h"
#include "Arduino.h"

#ifdef __cplusplus
extern "C" {
#endif

const PinName digitalPin[] = {
//Digital outputs
  PA_1,                     //0
  PA_2,                     //1
  PA_3,                     //2
  PA_4,                     //3
  PA_6,                     //4
  PA_7,                     //5
  PB_0,                     //6
  PB_1,                     //7
  PB_2,                     //8
  PF_12,                    //9
  PF_13,                    //10
  PF_14,                    //11
  PF_15,                    //12
//Relays
  PG_0,                     //13  
  PG_1,                     //14 
  PE_7,                     //15              
  PE_8,                     //16            
  PE_9,                     //17                                                   
//Digital inputs               
  PE_1,                     //18
  PE_0,                     //19
  PB_9,                     //20
  PB_8,                     //21
  PB_7,                     //22
  PB_6,                     //23
  PB_5,                     //24
  PB_4,                     //25
  PB_3,                     //26
  PG_15,                    //27
  PG_14,                    //28
  PG_13,                    //29
  PG_12,                    //30
  PG_11,                    //31
  PG_10,                    //32
  PG_9,                     //33
  PD_7,                     //34
  PD_4,                     //35
//Analog inputs                
  PD_14,                    //36
  PD_13,                    //37
  PD_12,                    //38
  PD_11,                    //39
                               
//Comms                    
  PA_11,      //CAN_RX      //40
  PA_12,      //CAN_TX      //41
                               
  PC_12,      //SPI MOSI    //42
  PC_11,      //SPI MISO    //43
  PC_10,      //SPI CLK     //44
  PG_2,       //SPI CS1     //45
  PG_3,       //SPI CS2     //46
  PG_4,       //SPI CS3     //47
  PG_5,       //SPI CS4     //48
  PG_6,       //SPI CS5     //49
  PG_7,       //SPI CS6     //50
  PG_8,       //SPI CS7     //51
                                                                
  PA_10, //FTDI                        PD_9,  //UART3_RX st-link  //52
  PA_9,  //FTDI                        PD_8,  //UART3_TX st-link  //53

  PD_6,  //RS485                       //UART2_RX    //54
  PD_5,  //RS485                       //UART2_TX    //55

  //for testing only
  PC_13 //user button
};

/**
  * @brief  System Clock Configuration
  * @param  None
  * @retval None
  */
WEAK void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {};

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
	RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
}

#ifdef __cplusplus
}
#endif
