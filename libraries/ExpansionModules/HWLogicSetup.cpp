#include "HWLogicSetup.h"
#include "Arduino.h"
//#include "UARTLib.h"
#include "CANLib.h"

void setupPinModes();

void hwlogic_setup(){  
  //Serial.begin(9600);
  setupPinModes();
  CAN_Init();
}

void setupPinModes(){
  hwlogic_pinMode(OX0, OUTPUT);
  hwlogic_pinMode(OX1, OUTPUT);
  hwlogic_pinMode(OX2, OUTPUT);
  hwlogic_pinMode(OX3, OUTPUT);
  hwlogic_pinMode(OX4, OUTPUT);
  hwlogic_pinMode(OX5, OUTPUT);
  hwlogic_pinMode(OX6, OUTPUT);
  hwlogic_pinMode(OX7, OUTPUT);
  hwlogic_pinMode(OX8, OUTPUT);
  hwlogic_pinMode(OX9, OUTPUT);
  hwlogic_pinMode(OX10, OUTPUT);
  hwlogic_pinMode(OX11, OUTPUT);
  hwlogic_pinMode(OX12, OUTPUT);
  
  hwlogic_pinMode(R0, OUTPUT);
  hwlogic_pinMode(R1, OUTPUT);
  hwlogic_pinMode(R2, OUTPUT);
  hwlogic_pinMode(R3, OUTPUT);
  hwlogic_pinMode(R4, OUTPUT);
  
  hwlogic_pinMode(IX0, INPUT_PULLUP);
  hwlogic_pinMode(IX1, INPUT_PULLUP);
  hwlogic_pinMode(IX2, INPUT_PULLUP);
  hwlogic_pinMode(IX3, INPUT_PULLUP);
  hwlogic_pinMode(IX4, INPUT_PULLUP);
  hwlogic_pinMode(IX5, INPUT_PULLUP);
  hwlogic_pinMode(IX6, INPUT_PULLUP);
  hwlogic_pinMode(IX7, INPUT_PULLUP);
  hwlogic_pinMode(IX8, INPUT_PULLUP);
  hwlogic_pinMode(IX9, INPUT_PULLUP);
  hwlogic_pinMode(IX10, INPUT_PULLUP);
  hwlogic_pinMode(IX11, INPUT_PULLUP);
  hwlogic_pinMode(IX12, INPUT_PULLUP);
  hwlogic_pinMode(IX13, INPUT_PULLUP);
  hwlogic_pinMode(IX14, INPUT_PULLUP);
  hwlogic_pinMode(IX15, INPUT_PULLUP);
  hwlogic_pinMode(IX16, INPUT_PULLUP);
  hwlogic_pinMode(IX17, INPUT_PULLUP);
  
  hwlogic_pinMode(IW0, INPUT);
  hwlogic_pinMode(IW1, INPUT);
  hwlogic_pinMode(IW2, INPUT);
  hwlogic_pinMode(IW3, INPUT);
}

//TODO: Ethernet setup

