#include "CANLib.h"
#include "stm32_def.h"
#include "stm32f3xx_hal_conf.h"
#include "Arduino.h"

CAN_HandleTypeDef hcan;

void HAL_CAN_MspInit(CAN_HandleTypeDef* hcan)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  if(hcan->Instance==CAN)
  {
    /* Peripheral clock enable */
    __HAL_RCC_CAN1_CLK_ENABLE();

    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**CAN GPIO Configuration
    PB8     ------> CAN_RX
    PB9     ------> CAN_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_CAN;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* CAN interrupt Init */
    HAL_NVIC_SetPriority(USB_HP_CAN_TX_IRQn, 0, 0);
    HAL_NVIC_SetPriority(CAN_RX1_IRQn, 0, 0);

    HAL_NVIC_EnableIRQ(USB_HP_CAN_TX_IRQn);
    HAL_NVIC_EnableIRQ(CAN_RX1_IRQn);
  }
}
void HAL_CAN_MspDeInit(CAN_HandleTypeDef* hcan)
{
  if(hcan->Instance==CAN)
  {
    /* Peripheral clock disable */
    __HAL_RCC_CAN1_CLK_DISABLE();

    /**CAN GPIO Configuration
    PB8     ------> CAN_RX
    PB9     ------> CAN_TX
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_8|GPIO_PIN_9);

    /* CAN interrupt DeInit */
    HAL_NVIC_DisableIRQ(USB_HP_CAN_TX_IRQn);
    HAL_NVIC_DisableIRQ(USB_LP_CAN_RX0_IRQn);
	HAL_NVIC_DisableIRQ(CAN_RX1_IRQn);
  }
}

extern "C" void USB_HP_CAN_TX_IRQHandler(void)
{
  HAL_CAN_IRQHandler(&hcan);
}
extern "C" void CAN_RX1_IRQHandler(void)
{
  HAL_CAN_IRQHandler(&hcan);
}

void CAN_Init(){
	hcan.Instance = CAN;
	hcan.Init.Mode = CAN_MODE_NORMAL;

	hcan.Init.AutoBusOff = DISABLE;
	hcan.Init.AutoRetransmission = ENABLE;
	hcan.Init.AutoWakeUp = DISABLE;
	hcan.Init.ReceiveFifoLocked = DISABLE;
	hcan.Init.TimeTriggeredMode = DISABLE;
	hcan.Init.TransmitFifoPriority = DISABLE;

	hcan.Init.Prescaler = 9;
	hcan.Init.SyncJumpWidth = CAN_SJW_1TQ;
	hcan.Init.TimeSeg1 = CAN_BS1_13TQ;
	hcan.Init.TimeSeg2 = CAN_BS2_2TQ;

	if (HAL_CAN_Init(&hcan) != HAL_OK)
	{
		Error_Handler();
	}

	CAN_FilterTypeDef sFilterConfig;

	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0;
	sFilterConfig.FilterIdLow = 0;
	sFilterConfig.FilterMaskIdHigh = 0;
	sFilterConfig.FilterMaskIdLow = 0;
	sFilterConfig.FilterFIFOAssignment = CAN_FILTER_FIFO1;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.SlaveStartFilterBank = 14;

	if(HAL_CAN_ConfigFilter(&hcan, &sFilterConfig) != HAL_OK){
		Error_Handler();
	}

    if(HAL_CAN_ActivateNotification(&hcan, CAN_IT_TX_MAILBOX_EMPTY | CAN_IT_RX_FIFO1_MSG_PENDING) != HAL_OK){
		Error_Handler();
	}

	if(HAL_CAN_Start(&hcan) != HAL_OK){
		Error_Handler();
	}
}

void tx_who_is_here(){
	CAN_TxHeaderTypeDef TxHeader;
	uint32_t TxMailbox;

	TxHeader.ExtId = 0;
	TxHeader.IDE = CAN_ID_EXT;
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.DLC = 0;
	
	if(HAL_CAN_AddTxMessage(&hcan, &TxHeader, 0, &TxMailbox) != HAL_OK){
		Error_Handler();
	}

}
void tx_data(uint32_t frameID, uint8_t data[], uint32_t sizeOfArray, bool stdId){
    CAN_TxHeaderTypeDef TxHeader;
	uint32_t TxMailbox;
	
	if(sizeOfArray > 8) { return; } //TODO: Ni error sam povej da je overflow

	TxHeader.StdId = frameID;
	TxHeader.ExtId = frameID;
	TxHeader.IDE = stdId ? CAN_ID_STD : CAN_ID_EXT;
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.DLC = sizeOfArray;
	
	if(HAL_CAN_AddTxMessage(&hcan, &TxHeader, data, &TxMailbox) != HAL_OK){
		Error_Handler();
	}

	/* //waiting for message to leave
	Serial.println("pending..");
	while(HAL_CAN_IsTxMessagePending(&hcan, TxMailbox));

	//waiting for transmission request to be completed by checking RQCPx
	Serial.println("trq completed..");
	while( !(hcan.Instance->TSR & ( 0x1 << (7 * ( TxMailbox - 1 )))));

	//checking if there is an error at TERRx, may be done with TXOKx as well (i think)
	if ((hcan.Instance->TSR & ( 0x8 << (7 * ( TxMailbox - 1 ))))){
		//error is described in ESR at LEC last error code
		uint8_t error_code = (hcan.Instance->ESR & 0x70 ) >> 4;
		Serial.print("error:");
		Serial.println(error_code);
		//000: No Error
		//001: Stuff Error
		//010: Form Error
		//011: Acknowledgment Error
		//100: Bit recessive Error
		//101: Bit dominant Error
		//110: CRC Error
		//111: Set by software
		while (1);
	} */
}
void tx_remote(uint32_t frameID){
    CAN_TxHeaderTypeDef TxHeader;
	uint32_t TxMailbox;

	TxHeader.StdId = frameID;
	TxHeader.IDE = CAN_ID_STD;
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.DLC = 1; //todo da se updata

	if(HAL_CAN_AddTxMessage(&hcan, &TxHeader, 0, &TxMailbox) != HAL_OK){
		Error_Handler();
	}
}