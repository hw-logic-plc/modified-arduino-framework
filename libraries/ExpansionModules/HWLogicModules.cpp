#include "HWLogicModules.h"
#include "Arduino.h"
#include "CANLib.h"

Module::Module(uint32_t ip){
    set_IP(ip);
}

uint32_t Module::get_IP() { 
    return IP; 
}
void Module::set_IP(uint32_t newIP) { 
    if(newIP < MAX_IP_ADDRESS) { 
        IP = newIP; 
    } 
    else { 
        IP = 0; /*Throw error*/
    } 
}

//********OutputModule************

OutputModule::OutputModule(uint32_t ip) : Module(ip){ }

void OutputModule::digitalWrite(uint8_t pin, uint8_t val){
    uint8_t data[3] = {0, pin, val};
    tx_data(IP, data, 3);
}

void OutputModule::analogWrite(uint8_t pin, uint8_t val){
    uint8_t data[3] = {1, pin, val};
    tx_data(IP, data, 3);
}

//********InputModule************

InputModule::InputModule(uint32_t ip) : Module(ip){ }

//********Can Bus Functions************

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan){
  CAN_RxHeaderTypeDef RxHeader;
  uint8_t received_msg[8];

  if(HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO1, &RxHeader, received_msg) != HAL_OK){
    Error_Handler();
  }

  uint32_t mac = (received_msg[0] << 24) + (received_msg[1] << 16) + (received_msg[2] << 8) + (received_msg[3] << 0);
  uint32_t ip = (received_msg[4] << 8) + (received_msg[5] << 0);
  uint32_t type = received_msg[6];  

  Serial.print('G');
  Serial.print(mac, HEX);
  Serial.print(' ');
  Serial.print(ip);
  Serial.print(' ');
  Serial.print(type);
  Serial.print('\n');
}

void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan){
  UNUSED(hcan);
  //Serial.begin(9600);
  //Serial.println("Transmitted from MB0");
}