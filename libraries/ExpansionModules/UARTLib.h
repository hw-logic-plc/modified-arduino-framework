#ifndef _UART_LIBRARY_
#define _UART_LIBRARY_

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void USART2_Init();

#ifdef __cplusplus
} // extern "C"
#endif

#endif