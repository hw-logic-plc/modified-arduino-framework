#ifndef _CAN_LIBRARY_
#define _CAN_LIBRARY_

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include "stm32f3xx_hal_can.h"

void USB_HP_CAN_TX_IRQHandler(void);
void CAN_RX1_IRQHandler(void);

void CAN_Init();

void tx_who_is_here();
void tx_data(uint32_t frameID, uint8_t data[], uint32_t sizeOfArray, bool stdId=true);
void tx_remote(uint32_t frameID);

#ifdef __cplusplus
} // extern "C"
#endif

#endif