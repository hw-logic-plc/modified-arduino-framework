#ifndef _HWLOGICMODULES_
#define _HWLOGICMODULES_

#include "stm32_def.h"

#define MAX_IP_ADDRESS 0x7FF

class Module {
protected:
    uint32_t IP;
    
public:
    Module(uint32_t ip);
    uint32_t get_IP();
    void set_IP(uint32_t newIP);
};

class OutputModule : public Module {
public:
    OutputModule(uint32_t ip);

    void digitalWrite(uint8_t pin, uint8_t val);
    void analogWrite(uint8_t pin, uint8_t val);
};

class InputModule : public Module {
public:
    InputModule(uint32_t ip);

    void digitalRead(uint8_t pin, uint8_t val);
    void analogRead(uint8_t pin, uint8_t val);
};

#endif